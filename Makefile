
build_all_win: install_package_win run_unittest_win run_e2etest_win build_app_win
build_all: install_package run_unittest run_e2etest build_app

install_package_win:
	npm install
	npm run postinstall

run_unittest_win:
	npm run test-win

run_e2etest_win:
	echo "Test e2e"

build_app_win:
	npm run app:dist-win
	tar.exe -a -c -f app.zip dist

install_package:
	npm install

run_unittest:
	npm test

run_e2etest:
	echo "Test e2e"

build_app:
	npm run app:dist
	tar.exe -a -c -f app.zip dist
